## "Cumplimiento - MEBR - Web - Front" - AppEngine

## Información del proyecto

``` 
- Tarea: Manual
- RFC: RFC0067705
- Descrpción del cambio: Configurar Harness
- Equipo: (Cumplimiento) Geyse Morales Pitalua
- Proyecto GCP: gnp-dlk-cumplimiento-[qa/uat/pro]
- Nombre del Proyecto GNP: Metodología de Evaluación Basada en Riesgo - Fase 2
- Tablas Afectadas: N/A
- DAG o Proceso Afectado: N/A
- Día/Horario en que ejecuta el DAG: N/A
``` 


## Redacción de la tarea
```
Configurar Harness para la CI de este componente.
Utilizar los archivos en raíz:
app.yaml
```

## Comando utilizado para el deploy
npm install
ng serve
gcloud app deploy --project=gnp-dlk-cumplimiento-qa

