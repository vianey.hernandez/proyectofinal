import { Component, Inject, OnInit } from '@angular/core';
import {
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-pop-up',
  templateUrl: './pop-up.component.html',
  styleUrls: ['./pop-up.component.sass'],
})
export class PopUpComponent implements OnInit {
  form!: FormGroup;
  columnas: any[] = [];
  title: any
  constructor(
    public dialogRef: MatDialogRef<PopUpComponent>,
    private fb: FormBuilder,

    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.columnas = data.columnas;
    this.title=data.titulo;
    console.log(data.titulo);
    dialogRef.disableClose = true;
    this.setConfig(this.columnas);
  }


  setConfig(columnas: any[]) {
    let arr: { [key: string]: any } = {};
      columnas.forEach(function (arrayItem) {
        if(arrayItem.type=='int'){
          arr[arrayItem.name] = ['',[Validators.required, Validators.pattern("^[0-9]+$")]];
        }else if(arrayItem.type == 'float'){
          arr[arrayItem.name] = ['',[Validators.required, Validators.pattern("^([0-9]+([.][0-9]*)?|[.][0-9]+)$")]];
        }else if(arrayItem.type=='string'){
          arr[arrayItem.name] = ['',[Validators.required, Validators.pattern('^[A-Za-z0-9.,-?¿!¡()</üÜñÑáéíóúÁÉÍÓÚ ]+$')]]; 
        }
      });
    this.form = this.fb.group(arr);
  }

  ngOnInit(): void {}
  onNoClick(): void {
    this.dialogRef.close();
    this.dialogRef.disableClose = false;
  }
  save() {
    this.dialogRef.close(this.form.value);
  }
}
