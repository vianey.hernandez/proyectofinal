import { Component, OnInit, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatTabsModule } from '@angular/material/tabs';
import { ThemePalette } from '@angular/material/core';
import { DataService } from 'src/app/services/data.service';
import { catchError } from 'rxjs/internal/operators/catchError';
import { throwError } from 'rxjs/internal/observable/throwError';
import { LoaderService } from 'src/app/services/loader.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-catalogues',
  templateUrl: './catalogues.component.html',
  styleUrls: ['./catalogues.component.sass'],
})
export class CataloguesComponent implements OnInit{
  dataArray: any; //variable auxiliar
  size: any; // tamaño del Array JSON
  background: ThemePalette = undefined;
  activeLink!: any;
  cat: any;
  @Input() visualizar = true;
  isLoading: boolean = false;
  constructor(private data: DataService,public loaderService: LoaderService) {}


  ngOnInit(): void {
    this.data
      .getData()
      .pipe(
        catchError((err) => {
          console.log('Handling error locally and rethrowing it...', err);
          return throwError(err);
        })
      )
      .subscribe(
        (res) => {
          this.dataArray = res;
          this.size = Object.keys(this.dataArray.catalogos).length;
          this.cat = this.dataArray.catalogos;
        },
        (err: HttpErrorResponse) => {
          console.log(err);
        }
      );
  }

}
