import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportePepComponent } from './reporte-pep.component';

describe('ReportePepComponent', () => {
  let component: ReportePepComponent;
  let fixture: ComponentFixture<ReportePepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportePepComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportePepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
