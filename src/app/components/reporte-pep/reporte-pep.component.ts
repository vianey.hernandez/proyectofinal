import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reporte-pep',
  templateUrl: './reporte-pep.component.html',
  styleUrls: ['./reporte-pep.component.sass']
})
export class ReportePepComponent implements OnInit {
  proyecto="gnp-dlk-cumplimiento-pro";
  Dataset="cumplimiento_mebr";
  Envista="cnm_base_consolidada";

  constructor() { }

  ngOnInit(): void {
  }

  onclick(){
    alert("Generando reporte" + "\n" + "Recibirá un correo cuando la ejecución del mismo haya terminado")
  }
}