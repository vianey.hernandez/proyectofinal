import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { LoaderService } from 'src/app/services/loader.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.sass'],
})
export class NavigationComponent implements OnInit {
  fullName = 'Juan Pérez';
  intials!: any;
  @ViewChild('icon') icon!: ElementRef;
  auxiliar!: any;
  catalog: any;
  isLoading: boolean = false;
  permiso!: any;
  editar !: boolean;
  //creacion de arreglo de permisos
  nuevoPermiso: any = [];
  //para mostrar en tabla se crea un objeto
  componentes = [
    { columna: 'MEBR', icono: 'fa fa-briefcase', link: '/membr', valor: 1 },
    { columna: 'Base Consolidada', icono: 'fa fa-info-circle', link: '/baseconsolidada', valor: 1 },
    { columna: 'Reporte PEP', icono: 'fa fa-chart-bar', link: '/reportepep', valor: 1 },
    { columna: 'Catálogos', icono: 'fa fa-user-friends', link: '/catalogos', valor: 1 }
  ];

  mostrarArreglo: any = [];

  constructor(private data: DataService, public loaderService: LoaderService) {}

  ngOnInit(): void {
    this.intials = this.fullName
      .split(' ')
      .map((name) => name[0])
      .join('')
      .toUpperCase();
    this.icon = this.intials;
    
    this.data.getCatalogo(1).subscribe((res) => {
      this.auxiliar = res;
      this.catalog = this.auxiliar.catalog;
      this.permiso = this.catalog.canModifyRows;
      this.permiso = '11111';
      this.data.rolEditar = this.permiso;
      for (let i = 0; i < this.permiso.length; i++) {
        this.nuevoPermiso.push(parseInt(this.permiso[i]));
      }
      if(this.nuevoPermiso.length-1 == 1){
        this.editar=true;
      }else{
        this.editar=false;
      }
    });
  }
}
