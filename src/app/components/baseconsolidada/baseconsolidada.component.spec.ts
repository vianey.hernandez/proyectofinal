import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseconsolidadaComponent } from './baseconsolidada.component';

describe('BaseconsolidadaComponent', () => {
  let component: BaseconsolidadaComponent;
  let fixture: ComponentFixture<BaseconsolidadaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseconsolidadaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseconsolidadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
