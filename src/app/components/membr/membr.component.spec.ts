import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MembrComponent } from './membr.component';

describe('MembrComponent', () => {
  let component: MembrComponent;
  let fixture: ComponentFixture<MembrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MembrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MembrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
