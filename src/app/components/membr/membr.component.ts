import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-membr',
  templateUrl: './membr.component.html',
  styleUrls: ['./membr.component.sass']
})
export class MembrComponent implements OnInit {
  caja1=""; 
  fechaOne="";
  fechaTwo= "";

  constructor() { }

  ngOnInit(): void {
  }

  fecha1(fechauno: any, fechados: any, caja: any ){
    if(this.caja1 =="" || this.fechaOne==""  || this.fechaTwo==""){
     alert("debes de completar todos los campos");
  }else if(this.fechaOne > this.fechaTwo){
      alert("el fin rango debe ser superios al inicio rango")
      fechados.reset();
    }else{
      alert("registro capturado: " + this.caja1);
      fechauno.reset();
      fechados.reset();
      caja.reset();
  }
}

}
