import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DataService } from 'src/app/services/data.service';
import { PopUpComponent } from '../pop-up/pop-up.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import {
  FormBuilder,
  Validators,
  FormGroup,
  FormControl,
  FormArray,
  AbstractControl,
} from '@angular/forms';
import { LoaderService } from 'src/app/services/loader.service';
import { Subject } from 'rxjs';
import swal from 'sweetalert2';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.sass'],
})
export class TableComponent implements OnInit {
  auxiliar: any;
  displayedColumns: any[] = [];
  columnas: any[] = [];
  dataSource!: MatTableDataSource<any>;
  agregarCelda: any;
  celdas!: any[];
  auxCeldas!: any;
  auxCol!: any;
  displayedHead: string[] = [];
  myformArray: any;
  guardarColumna = 0;
  valido = true;
  titulo: any;
  isLoading: boolean = false;
  rolEditar :any;
  @ViewChild('myInp', { static: true }) myInputVariable!: ElementRef;

  @ViewChild(MatPaginator, { static: true }) paginator!: MatPaginator;

  constructor(
    private consultarDatos: DataService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    public loaderService: LoaderService,
    private router: Router
  ) {
    this.rolEditar= this.consultarDatos.rolEditar;
    console.log(this.rolEditar);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.Obtenerdatos(params['id']);
    });
  }
  enviarDatos():void{ 
    this.consultarDatos.addCatalogo(this.myformArray?.value);
  }
  actualizarDatos():void{
    this.consultarDatos.updateCatalgo(this.myformArray?.value);
  }
  Obtenerdatos(id: any): void {
    this.myInputVariable.nativeElement.value = '';
    this.consultarDatos.getCatalogo(id).subscribe((res) => {
      this.myformArray = new FormArray([]);
      this.auxiliar = res;
      this.auxCeldas = this.auxiliar.data;
      this.auxCol = this.auxiliar.catalog;
      this.titulo = this.auxCol.title;
      this.columnas = this.auxCol.columns;
      this.displayedColumns = this.columnas.map((c) => c.name);
      this.displayedHead = [...this.displayedColumns];
      this.agregarCelda = this.auxCol.canModifyRows;
      if (this.agregarCelda == true) {
        this.displayedHead.push('delete');
      }
      this.columnas.map((data) => {
        if (data.canModify == true) this.guardarColumna = 1;
      });
      this.auxCeldas.forEach((obj: any) => {
        this.addForm(obj);
      });
      this.dataSource = new MatTableDataSource<any>(this.myformArray.controls);

      this.dataSource.filter = '';
      const filterPredicate = this.dataSource.filterPredicate;
      this.dataSource.filterPredicate = (
        data: AbstractControl,
        filter: any
      ) => {
        return filterPredicate.call(this.dataSource, data.value, filter);
      };
    });
    console.log("datos",)
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  validar() {
    for (let i = 0; i < this.myformArray.length; i++) {
      if (this.myformArray.controls[i].status != 'VALID') {
        this.valido = false;
      } else this.valido = true;
    }
    console.log(this.valido);
    if (this.valido == true) {
      
      let timerInterval: any;
      swal.fire({
        title: 'Completado',
        text: 'Datos enviados',
        icon: 'success',
        width: 400,
        timer: 1500,
        timerProgressBar: true,
        didOpen: () => {
          swal.showLoading();
          timerInterval = setInterval(() => {
            swal.getTimerLeft();
          }, 100);
        },
        willClose: () => {
          clearInterval(timerInterval);
          this.router.navigate(['catalogos']);
        },
      });
    } else {
      swal.fire('Error', 'Verifique los datos', 'error');
    }
  }

  delete(index: number) {
    this.myformArray.removeAt(index);
    this.dataSource = new MatTableDataSource<any>(this.myformArray.controls);
  }

  addForm(obj: any) {
    const newGroup = new FormGroup({});
    this.columnas.forEach((x) => {
      if (x.type == 'int') {
        newGroup.addControl(
          x.name,
          new FormControl(obj[x.name], [
            Validators.required,
            Validators.pattern('^[0-9]+$'),
          ])
        );
      } else if (x.type == 'float') {
        newGroup.addControl(
          x.name,
          new FormControl(obj[x.name], [
            Validators.required,
            Validators.pattern('^([0-9]+([.][0-9]*)?|[.][0-9]+)$'),
          ])
        );
      } else if (x.type == 'string') {
        newGroup.addControl(
          x.name,
          new FormControl(obj[x.name], [
            Validators.required,
            Validators.pattern('^[A-Za-z0-9.,-?¿!¡()üÜ</ñÑáéíóúÁÉÍÓÚ ]+$'),
          ])
        );
      }
    });
    this.myformArray.push(newGroup);
    this.dataSource = new MatTableDataSource<any>(this.myformArray.controls);
  }

  //Empieza funcionamiento de ventana pop-up
  openDialog(): void {
    const dialogRef = this.dialog.open(PopUpComponent, {
      disableClose: true,
      data: { columnas: this.columnas, titulo: this.titulo },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        console.log('Dialog output:', result);
        this.addForm(result);
      }
    });
  }
}
