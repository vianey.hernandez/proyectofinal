import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { TableComponent } from './components/table/table.component';
import { PopUpComponent } from './components/pop-up/pop-up.component';
import { CataloguesComponent } from './components/catalogues/catalogues.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { FooterComponent } from './components/footer/footer.component';
//Componentes --Carlos
import { BaseconsolidadaComponent } from './components/baseconsolidada/baseconsolidada.component';
import { ReportePepComponent } from './components/reporte-pep/reporte-pep.component';
import { MembrComponent } from './components/membr/membr.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { CdkTableModule } from '@angular/cdk/table';
import {MatTooltipModule} from '@angular/material/tooltip';
import { CookieService } from 'ngx-cookie-service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

//Agregando libreria para datapicker --Carlos
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { registerLocaleData } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { LOCALE_ID } from '@angular/core';
import { LoaderInterceptor } from './services/loader.interceptor';
import { ErrorInterceptor } from './services/error.interceptor';
import { JwtInterceptor } from './services/jwt.interceptor';

registerLocaleData(localPy,'es');


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    TableComponent,
    PopUpComponent,
    CataloguesComponent,
    HomePageComponent,
    FooterComponent,
    BaseconsolidadaComponent,
    ReportePepComponent,
    MembrComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatPaginatorModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatTabsModule,
    MatDialogModule,
    ReactiveFormsModule,
    FormsModule,
    CdkTableModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    //subiendo datos para datapicker
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatSelectModule
  ],
  providers: [CookieService,{ provide: LOCALE_ID, useValue: 'es-es' },{ provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }, { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },{ provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },],
  bootstrap: [AppComponent],
})
export class AppModule { }

//funcion para pasar datos al español del datapicker --Carlos
function localPy(localPy: any, arg1: string) {
  throw new Error('Function not implemented.');
}