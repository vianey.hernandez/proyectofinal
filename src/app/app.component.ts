import { Component } from '@angular/core';
import { environment } from '../environments/environment.kua';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'proyectoFinal';
  public env: string = environment.env;
}
