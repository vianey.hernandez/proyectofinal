import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from './authentication.service';


@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        const user = this.authenticationService.userValue;

        if (user) {
            console.log('auth.guard',user);
            if (route.data['roles'] && route.data['roles'].indexOf(user.role) === -1) {
                this.router.navigate(['/']);
                console.log('auth.guard 2 ',user.role)
                return false;
            }else{
                console.log('auth.guard  ',route.data['roles'])
                return true;
            }
        }else{
            console.log('no hay nada');
        }

        this.router.navigate(['/'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}