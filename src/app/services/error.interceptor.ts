import { DataService } from 'src/app/services/data.service';

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private data: DataService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        if ([401, 403, 500, 400, 404].indexOf(err.status) !== -1) {
          this.data.errorLink(err.status);
        } else {
          this.data.errorLink('Algo salio mal');
        }
        const error = err.error.message || err.statusText;
        return throwError(error);
      })
    );
  }
}
