
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  private userSubject: BehaviorSubject<any>;
  public user: Observable<any>;

  constructor(private cookieService: CookieService,private router: Router, private http: HttpClient) {
    this.userSubject = new BehaviorSubject<any>(
      JSON.parse(localStorage.getItem('user')!)
    );
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): any {
    return this.userSubject.value;
  }

  authenticate() {
    const token = this.cookieService.get('name');
    const helper = new JwtHelperService();
    const decoded = helper.decodeToken(token);
    return (decoded);
  }

  login() {
    localStorage.setItem('user', JSON.stringify(this.authenticate()));
    this.userSubject.next(this.authenticate());
    return this.authenticate();
  }
  
  /* 
  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
    this.userSubject.next(null);
    this.router.navigate(['/']);
  } */
}

