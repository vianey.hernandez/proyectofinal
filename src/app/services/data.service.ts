import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private apiUrl =
    'https://kumma-mebr-webadmin-backend-dot-kumma-cloud.uc.r.appspot.com/catalogo';
  private apiUrl2 =
    'https://kumma-mebr-webadmin-backend-dot-kumma-cloud.uc.r.appspot.com/catalogos';
  private roles !:any;
  constructor(private http: HttpClient, private router: Router) {}

  getData(): Observable<any[]> {
    return this.http.get<any[]>(this.apiUrl2);
  }

  getCatalogo(id: number): Observable<any[]> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<any[]>(url);
  }

  set rolEditar(rol:any){
    this.roles = rol;
  }

  get rolEditar():string{
    return this.roles;
  }

  addCatalogo(data:any): Observable<any>{
    return this.http.post<any>('url',data);
  }
  updateCatalgo(data:any):Observable<any>{
    return this.http.put<any>('url',data);
  }
  errorLink(obj: any) {
    let timerInterval: any;
    setTimeout(() => {
      swal.fire({
        title: 'Ops...',
        text: obj,
        icon: 'error',
        width: 350,
        timer: 2500,
        timerProgressBar: true,
        didOpen: () => {
          swal.showLoading();
          timerInterval = setInterval(() => {
            swal.getTimerLeft();
          }, 100);
        },
        willClose: () => {
          clearInterval(timerInterval);
          this.router.navigate(['/']);
        },
      });
    },2500);
  }
}
