import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const user = this.authenticationService.userValue;
    console.log('user jwt.interceptor', user);
    const isApiUrl = request.url.startsWith(
      'https://kumma-mebr-webadmin-backend-dot-kumma-cloud.uc.r.appspot.com/'
    );
    const headers = new HttpHeaders({
      Authorization: `${user.token}`,
      'WEB-API-key':
        'https://kumma-mebr-webadmin-backend-dot-kumma-cloud.uc.r.appspot.com/',
      'Content-Type': 'application/json',
    });

    if (isApiUrl) {
      request = request.clone({ headers });
      console.log(request);
    }
    console.log('nada');
    return next.handle(request);
  }
}
