import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { CataloguesComponent } from './components/catalogues/catalogues.component';
import { TableComponent } from './components/table/table.component';

//Agregando componentes -Carlos
import { BaseconsolidadaComponent } from './components/baseconsolidada/baseconsolidada.component';
import { MembrComponent } from './components/membr/membr.component';
import { ReportePepComponent } from './components/reporte-pep/reporte-pep.component';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {path:'', component: HomePageComponent },
  {path: 'baseconsolidada',  component: BaseconsolidadaComponent},
  {path: 'reportepep',  component: ReportePepComponent},
  {path: 'membr',  component: MembrComponent},
  {path:'catalogos', component: CataloguesComponent,canActivate: [AuthGuard], data: { roles: 'Admin'}},
  {path:'catalogo/:id', component: TableComponent},
  {path: '**',redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
