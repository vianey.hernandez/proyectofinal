export interface  IColumnas{
    id: string,
    type: string,
    validation: boolean
    editable: boolean
}
export interface ICeldas{
    id_pais: number,
    cve_pais: string,
    desc_pais_original: string,
    desc_pais: string,
    ind_indice: number
} 
interface ICatalogo{
    
}
export interface ValidateData {
    id: number;
    title: string,
    gsURL: string,
    dbURL: string,
    addRows: boolean,
    columnas: IColumnas[],
    celdas: ICeldas[]
} 